package main

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

var opts struct {
	EnvPrefix string
	Dir       string
	File      string
	URL       string
	Writes    cli.StringSlice
	Debug     bool
}

func main() {
	app := cli.NewApp()
	app.Name = "launch"
	app.Version = "latest"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "prefix,p",
			Value:       "X",
			EnvVar:      "LAUNCH_PREFIX",
			Usage:       "prefix for environment variables",
			Destination: &opts.EnvPrefix,
		},
		cli.StringFlag{
			Name:        "dir,d",
			Usage:       "Specify directory containing property files (.properties) for interpolation",
			EnvVar:      "LAUNCH_DIR",
			Destination: &opts.Dir,
		},
		cli.StringFlag{
			Name:        "file,f",
			Usage:       "specify",
			EnvVar:      "LAUNCH_FILE",
			Destination: &opts.File,
		},
		cli.StringFlag{
			Name:        "url,u",
			Usage:       "Specifies url that can be used to pull properties content from",
			EnvVar:      "LAUNCH_URL",
			Destination: &opts.URL,
		},
		cli.StringSliceFlag{
			Name:   "write,w",
			EnvVar: "LAUNCH_WRITES",
			Usage:  "Comma separate list of template:target directory pairs e.g. /templates/conf:/tomcat/conf,/templates/lib:/apps/WEB-INF/classes",
			Value:  &opts.Writes,
		},
		cli.BoolFlag{
			Name:        "debug",
			Usage:       "emit debug messages",
			Destination: &opts.Debug,
		},
	}
	app.Usage = "write configuration files then launch app"
	app.Action = launch
	err := app.Run(os.Args)
	if err != nil {
		log.Fatalln(err)
	}
}

func launch(c *cli.Context) error {
	// build up our properties
	p := Properties{}

	if opts.Dir != "" {
		v, err := parseDir(opts.Dir)
		if err != nil {
			log.Println(err)
		}
		p.addAll(v)
	}

	if opts.File != "" {
		v, err := parseFile(opts.File)
		if err != nil {
			log.Println(err)
		}
		p.addAll(v)
	}

	if opts.URL != "" {
		v, err := parseUrl(opts.URL)
		if err != nil {
			log.Println(err)
		}
		p.addAll(v)
	}

	if v := parseEnv(opts.EnvPrefix); len(v) != 0 {
		p.addAll(v)
	}

	for _, all := range opts.Writes {
		for _, kv := range strings.Split(all, ",") {
			segments := strings.SplitN(kv, ":", 2)
			if len(segments) != 2 {
				return errors.Errorf("illegal write location, %v - want {template-dir}:{target-dir}", kv)
			}

			var (
				source = segments[0]
				target = segments[1]
			)

			if err := renderDir(os.Stdout, p, source, target); err != nil {
				return err
			}
		}
	}

	if opts.Debug {
		var keys []string
		for k := range p {
			keys = append(keys, k)
		}
		sort.Strings(keys)

		fmt.Println("Properties:")
		for _, key := range keys {
			fmt.Printf("%v: %v\n", key, p[key])
		}
	}

	if len(c.Args()) == 0 {
		return nil // nothing to execute
	}

	cmd := exec.Command(c.Args()[0], c.Args()[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

type Properties map[string]string

func (p Properties) addAll(in Properties) {
	if p == nil {
		return
	}

	for k, v := range in {
		p[k] = v
	}
}

var reKey = regexp.MustCompile(`^\s*([^: \t]+)\s*(:=\s*(.*)\s*$)?`)

// expand returns the value associated with str provided.
// If the str is of the format key:=default, then the default
// value will be returned if the key is not present
func (p Properties) expand(str string) (string, bool) {
	if p == nil {
		return "", false
	}

	matches := reKey.FindStringSubmatch(str)
	if len(matches) == 0 {
		return "", false
	}

	key := matches[1]
	if v, ok := p[key]; ok {
		return v, true
	}

	if len(matches) == 4 {
		if defaultValue := matches[3]; len(matches[2]) > 0 {
			return defaultValue, true
		}
	}

	return "", false
}

// parse Java style properties
func parse(r io.Reader) (Properties, error) {
	p := Properties{}
	buf := bytes.NewBuffer(nil)
	reader := bufio.NewReader(r)
	for {
		line, prefix, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, errors.Wrap(err, "unable to parse properties")
		}

		buf.Reset()
		buf.Write(line)
		if prefix {
			continue
		}

		if k, v, ok := extractKV(buf.String()); ok {
			p[k] = v
		}
	}

	return p, nil
}

func parseFile(filename string) (Properties, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to open file, %v", filename)
	}
	defer f.Close()

	return parse(f)
}

func parseEnv(prefix string) Properties {
	prefix = strings.ToUpper(prefix)
	if !strings.HasSuffix(prefix, "_") {
		prefix += "_"
	}

	p := Properties{}
	for _, kv := range os.Environ() {
		if !strings.HasPrefix(kv, prefix) {
			continue
		}

		segments := strings.SplitN(kv[len(prefix):], "=", 2)

		key := segments[0]
		key = strings.Replace(key, "_", ".", -1) // convert _ to .
		key = strings.ToLower(key)

		value := segments[1]

		p[key] = value
	}

	return p
}

var reSchema = regexp.MustCompile(`^[^:]+://`)

const defaultSchema = "http://"

func parseUrl(url string) (Properties, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if !reSchema.MatchString(url) {
		url = defaultSchema + url
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, errors.Wrapf(err, "invalid url, %v", url)
	}
	req = req.WithContext(ctx)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to read content from url, %v", url)
	}
	defer resp.Body.Close()

	return parse(resp.Body)
}

func parseDir(dir string) (Properties, error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to read contents of dir, %v", dir)
	}

	properties := Properties{}
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		if !strings.HasSuffix(file.Name(), ".properties") {
			continue
		}

		path := filepath.Join(dir, file.Name())
		p, err := parseFile(path)
		if err != nil {
			return nil, err
		}
		properties.addAll(p)
	}

	return properties, nil
}

var (
	reIgnore     = regexp.MustCompile(`^\s*(#.*)?$`)
	reWhitespace = regexp.MustCompile(`\s`)
)

func extractKV(in string) (key, value string, ok bool) {
	if reIgnore.MatchString(in) {
		return "", "", false
	}

	segments := strings.SplitN(in, "=", 2)
	if len(segments) != 2 {
		return "", "", false
	}

	k := strings.TrimSpace(segments[0])
	v := strings.TrimSpace(segments[1])

	if reWhitespace.MatchString(k) {
		return "", "", false
	}

	return k, v, true
}

var (
	reMagic = regexp.MustCompile(`\${([^{}]+)}`)
)

func renderTemplate(p Properties, r io.Reader) (string, error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return "", errors.Wrapf(err, "unable to read content")
	}

	content, _, err := interpolateNested(p, string(data))
	if err != nil {
		return "", err
	}

	return content, nil
}

func renderDir(w io.Writer, p Properties, source, target string) error {
	fmt.Fprintf(w, "rendering '%v' => '%v'\n", source, target)

	return filepath.Walk(source, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		var (
			input  = filepath.Join(source, info.Name())
			output = filepath.Join(target, info.Name())
			dir    = filepath.Dir(output)
		)

		if err := os.MkdirAll(dir, 0755); err != nil {
			return errors.Wrapf(err, "unable to create directory, %v", dir)
		}

		fmt.Fprintf(w, "%v => %v\n", input, output)
		f, err := os.Open(input)
		if err != nil {
			return errors.Wrapf(err, "unable to read file, %v", input)
		}
		defer f.Close()

		content, err := renderTemplate(p, f)
		if err != nil {
			return errors.Wrapf(err, "unble to render template")
		}

		if err := ioutil.WriteFile(output, []byte(content), 0644); err != nil {
			return errors.Wrapf(err, "unable to write file, %v", output)
		}

		return nil
	})
}

func interpolate(p Properties, content string) (string, int, error) {
	n := 0
	matches := reMagic.FindAllStringSubmatch(content, -1)
	for _, match := range matches {
		if value, ok := p.expand(match[1]); ok {
			content = strings.Replace(content, match[0], value, -1)
			n++
		}
	}

	return content, n, nil
}

func interpolateNested(p Properties, content string) (string, int, error) {
	total := 0
	for {
		v, n, err := interpolate(p, content)
		if err != nil {
			return "", 0, err
		}

		content = v
		total += n

		if n == 0 {
			break
		}
	}

	return content, total, nil
}
