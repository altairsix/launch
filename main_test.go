package main

import (
	"testing"

	"strings"

	"os"

	"encoding/xml"

	"io/ioutil"

	"github.com/tj/assert"
)

func TestInterpolate(t *testing.T) {
	testCases := map[string]struct {
		Content    string
		Properties Properties
		Want       string
		N          int
	}{
		"simple": {
			Content:    `${hello}`,
			Properties: Properties{"hello": "world"},
			Want:       "world",
			N:          1,
		},
		"with default": {
			Content:    `${hello:=world}`,
			Properties: Properties{},
			Want:       "world",
			N:          1,
		},
		"empty default": {
			Content:    `${hello:=}`,
			Properties: Properties{},
			Want:       "",
			N:          1,
		},
		"no match": {
			Content:    `${catalina.base}`,
			Properties: Properties{},
			Want:       `${catalina.base}`,
			N:          0,
		},
		"nested": {
			Content:    `${foo:=${hello:=world}}`,
			Properties: Properties{},
			Want:       `${foo:=world}`,
			N:          1,
		},
	}

	for label, tc := range testCases {
		t.Run(label, func(t *testing.T) {
			content, n, err := interpolate(tc.Properties, tc.Content)
			assert.Nil(t, err)
			assert.Equal(t, tc.Want, content)
			assert.Equal(t, tc.N, n)
		})
	}
}

func TestParseDir(t *testing.T) {
	p, err := parseDir("testdata/properties")
	assert.Nil(t, err)
	want := Properties{
		"username": "default",
		"password": "password123",
		"blank_ok": "",
	}
	assert.Equal(t, want, p)
}

func TestInterpolateNested(t *testing.T) {
	testCases := map[string]struct {
		Content    string
		Properties Properties
		Want       string
		N          int
	}{
		"nest - 2": {
			Content:    `${foo:=${hello:=world}}`,
			Properties: Properties{},
			Want:       `world`,
			N:          2,
		},
		"nest - 3": {
			Content:    `${foo:=${hello:=${boo:=ya}}}`,
			Properties: Properties{},
			Want:       `ya`,
			N:          3,
		},
		"partial": {
			Content:    `${foo:=${hello:=${boo:=ya}}}`,
			Properties: Properties{},
			Want:       `ya`,
			N:          3,
		},
	}

	for label, tc := range testCases {
		t.Run(label, func(t *testing.T) {
			content, n, err := interpolateNested(tc.Properties, tc.Content)
			assert.Nil(t, err)
			assert.Equal(t, tc.Want, content)
			assert.Equal(t, tc.N, n)
		})
	}
}

func TestExpandNilProperties(t *testing.T) {
	var p Properties
	v, ok := p.expand("hello")
	assert.False(t, ok)
	assert.Zero(t, v)
}

func TestExpandMiss(t *testing.T) {
	p := Properties{"a": "b"}
	v, ok := p.expand("")
	assert.False(t, ok)
	assert.Zero(t, v)
}

func TestCopyAndInterpolate(t *testing.T) {
	p := Properties{
		"email": "user@example.com",
	}
	content := `To: ${email}`
	got, err := renderTemplate(p, strings.NewReader(content))
	assert.Nil(t, err)
	assert.Equal(t, "To: user@example.com", got)
}

func TestRenderDir(t *testing.T) {
	p := Properties{"username": "blah"}
	err := renderDir(ioutil.Discard, p, "testdata/templates", "target/output")
	assert.Nil(t, err)

	var expected struct {
		Properties []struct {
			Name  string `xml:"name,attr"`
			Value string `xml:"value,attr"`
		} `xml:"property"`
	}
	f, err := os.Open("target/output/sample.xml")
	assert.Nil(t, err)
	defer f.Close()

	assert.Nil(t, xml.NewDecoder(f).Decode(&expected))
	assert.Len(t, expected.Properties, 2)
	assert.Equal(t, expected.Properties[0].Name, "username")
	assert.Equal(t, expected.Properties[0].Value, "blah")
	assert.Equal(t, expected.Properties[1].Name, "password")
	assert.Equal(t, expected.Properties[1].Value, "junk")
}

func TestParseEnv(t *testing.T) {
	os.Setenv("BLAH_FOO_BAR", "true")
	p := parseEnv("blah")
	assert.Equal(t, "true", p["foo.bar"])
}
