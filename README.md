```bash
launch \
    --dir /etc/zap/conf.d \
    --write /opt/zaplabs/templates/tomcat:/opt/zaplabs/tomcat/conf \
    --write /opt/zaplabs/templates/WEB-INF:/opt/zaplabs/tomcat/cp/WEB-INF \
    -- \
    /opt/zaplabs/tomcat/bin/catalina.sh start
```

docker run -v properties:/etc/zap/conf.d

ZAP_FOO_BAR => foo.bar

registry/longisland:{version}
registry/longisland:latest
